<?php

return array(
    'host' => '',
    'username' => '',
    'password' => '',
    'database' => '',
    'debugging' => false,
    'http_server' => '',
    'mail_server' => '',
    'mail_auth' => true,
    'mail_user' => '',
    'mail_password' => '',
    'mail_sender_address' => '',
    'mail_sender_name' => '',
    'mail_subject' => '',
    'mail_recipient_address' => ''
);
