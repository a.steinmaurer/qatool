<?php

  require 'libraries/rb.php';
  require_once('libraries/PHPMailer/class.phpmailer.php');
  require_once('libraries/PHPMailer/class.smtp.php');
  $config = include('config.php');
  
  
  R::setup( "mysql:host=$config[host];dbname=$config[database]", $config["username"], $config["password"] );
  $server = $config['http_server'] . $_GET['cat'] . '/';

  R::fancyDebug( $config["debugging"] );
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>QA <?php echo $_GET['cat'] . "/" . $_GET['subcat'] ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <style>
    body{
      background-color:#1ABC9C;
    }

    #ask {
      background-color: #1ABC9C;
      color: white;
    }

    .answer {
      background-color: #e5e5e5;
    }

    a {
      color: #1ABC9C;
    }
  </style>
</head>
<body>

<?php

  if(isset($_POST['cat'])){
    $location = "";
    if(isset($_POST['subcat'])){
      $location = $config['http_server'] . $_POST['cat'] . '/' . $_POST['subcat'] ;
    } else {
      $location = $config['http_server'] . $_POST['cat'];
    }

    header("Location: " . $location);
  }
  

  if(empty($_GET))
  {
?>
  <div class="container mt-4">
    <div class="jumbotron">
      <h1 class="display-4 text-center">Q?A!</h1>
      <p class="lead text-center">Q?A! erleichtert die Kommunikation zwischen KursteilnehmerInnen und TrainerInnen - f&uuml;r ein erfolgreiches Miteinander!</p>
      <hr class="my-4">
      <p>Es funktioniert ganz einfach: KursteilnehmerInnen erhalten einen Link und k&ouml;nnen Fragen stellen. Die Fragen erscheinen entweder in einer kursinternen oder einer allgemeinen Gruppe. Die TrainerInnen k&ouml;nnen dann entweder w&auml;hrend des Kurses oder auch im Nachhinein darauf eingehen und diese (m&uuml;ndlich oder schriftlich) beantworten.</p>
    
      <hr class="my-4">
      <h2 class="text-center">Leg los!</h2>
      
      <form action="" method="POST">
        <div class="form-group form-check col-sm-3 text-center mx-auto">
          <div class="form-group">
            <label class="sr-only">Gruppe</label>
            <input type="text" name="cat" class="form-control form-control-lg" placeholder="Gruppe">
          </div>
          <div class="form-group">
            <label class="sr-only">Untergruppe</label>
            <input type="text" name="subcat" class="form-control form-control-lg" placeholder="Untergruppe">
          </div>
          <button type="submit" id="ask" class="btn btn-cyan btn-lg">Beitreten!</button>
        </div>  
      </form>
      
    </div>
  </div>
<?php
  } else {
?>

  <div class="container">
    <section id="cover">
      <div id="cover-caption">
        <div id="container" class="container">
          <form  action="" method="POST">
            <div class="row">
              <div class="col-12 text-center bg-light rounded mt-4">
              
                <h1 class="display-1" style="display:inline">Q</h1><h1 class="display-1" style="display:inline">?</h1> 
                
                <div class="form-inline justify-content-center mb-4 mt-4">
                  <div class="form-group">
                    <label class="sr-only">Frage</label>
                    <input type="text" name="question" class="form-control form-control-lg" placeholder="Wie lautet deine Frage...?">
                  </div>
                  <button type="submit" id="ask" class="btn btn-cyan btn-lg">Frag!</button>
                </div>  
                  
                <div class="form-group">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" name="public" id="publicQuestion">
                    <label class="form-check-label" for="publicQuestion">
                      <?php
                        echo "<a href='$server'>Anderen Kursen helfen?</a>";
                      ?>                    
                    </label>
                  </div>
                </div>
                    
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>


  <div class="container">
    <section id="cover">
      <div id="cover-caption">
        <div id="container" class="container">
          <div class="row">
            <div class="col-12 text-center bg-light rounded mt-4">
              <h1 class="display-1" style="display:inline">A</h1><h1 class="display-1" style="display:inline">!</h1>
                  <?php
                    if(isset($_GET['cat']) and $_GET['cat'] != '' and isset($_GET['subcat']) and $_GET['subcat'] != '')
                      $q = 'where category = \'' . $_GET['cat']. '\' and subcategory = \'' . $_GET['subcat']. '\' ORDER BY date DESC ';
                    else
                      $q = 'where category = \'' . $_GET['cat']. '\' and public = 1 ORDER BY date DESC';

                    $questions = R::find('question', $q);

                    foreach($questions as $q) {
                      echo '<div class="col-12 text-left rounded mb-4 border-bottom">';
                        echo date("d.m.Y H:i", strtotime($q->date));
                        echo '<h2>' . $q->text . '</h2>';
                          $answers = R::find('answer', ' question_id = ' . $q->id . ' ORDER BY date DESC');
                            
                          foreach($answers as $a){
                            echo '<div class="container">';
                              echo '<div class="col-12 text-left answer rounded mb-4">';
                                echo '<div class="row">';
                                  echo '<div class="col small">';
                                    echo '<span>' . date("d.m.Y H:i", strtotime($q->date)) . '</span>';

                                    echo '<span class="font-italic"> ' . $a->author . '</span>';
                                  echo '</div>';
                                echo '</div>';
                                echo '<div class="row">';
                                  echo '<div class="col">';
                                    echo $a->text;
                                  echo '</div>';
                                echo '</div>';
                              echo '</div>';
                            echo '</div>';
                          }
                        
                      echo '</div>';
                      
                    }
                  ?>
                
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php
    if(isset($_POST['question']) and $_POST['question'] != '') {
      
      $question = R::dispense('question');
      $question->text = $_POST['question'];
      $question->category = $_GET['cat'];
      $question->subcategory = $_GET['subcat'];
      $question->public = isset($_POST['public']) || $_GET['subcat'] == '' ? 1 : 0;
      
      if($config['mail_support']) {
        //Prepare all properties for the connection
        $mail = new PHPMailer(); 
        $mail->IsSMTP();
        $mail->Host = $config['mail_server'];
        $mail->SMTPDebug  = $config['debugging'];
        $mail->SMTPAuth = $config['mail_auth'];
        $mail->Username = $config['mail_user'];
        $mail->Password  = $config['mail_password'];
        $mail->CharSet ="UTF-8";
        
        //https://stackoverflow.com/questions/22927634/smtp-connect-failed-phpmailer-php
        $mail->SMTPKeepAlive = true;   
        $mail->Mailer = “smtp”; // don't change the quotes!

        //Compose email notification
        $mail->SetFrom($config['mail_sender_address'], $config['mail_sender_name']);
        $mail->AddAddress($config['mail_recipient_address']);
        $mail->Subject = $config['mail_subject'];
        $mail->Body = "New Message in " . $_GET['cat'] . "/" . $_GET['subcat'] . ": " . $_POST['question'];
        
        //Finally send the email
        $mail->Send();
        
      }
      $id = R::store($question);
      
      
      //Refresh webpage after submit
      echo "<meta http-equiv='refresh' content='0'>";
           
    }
  ?>

<?php
  }
?>
<nav class=" ">
    <div class="container text-center mt-2">
        <p class="col-md-12 col-sm-12 col-xs-12 bg-light"><a href="https://steinmaurer.cc" target="_blank">Alexander Steinmaurer</a> (2020)</p>
    </div>
</nav>
</body>
</html> 
