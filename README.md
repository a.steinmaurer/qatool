# QATool

Q?A! is a web-based tool for an easier communication between lectures and course participants. If questions appear during class and students are afraid to ask the tool can help. It is written in PHP and uses the ORM Framework RedBeanPhp. It also supports connecting to a SMTP server for notifications. 

## Getting Started

Simply clone the repository:

```
git clone https://gitlab.com/a.steinmaurer/qatool.git
```

### Prerequisites

To get the tool running PHP have to be installed (tested with PHP 7.2). RedBeanPhp is delivered within the repository. You also might want to set up a (MySQL) database. Bootstrap is provided via CDN - so it is not necessary to run it.

### Installing

After downloading/cloning the project the config.php file has to be modified. The configuration array contains different values that have to be adjusted:

| Entry      | Description                               | Value  |
| ---------- |:-----------------------------------------:|-------:| 
| host       | Hostname for the database server          | string |
| usename    | Username for database user                | string |
| password   | Corresponding password for db user        | string |
| database   | Name of database                          | string |
| debugging  | RedBeanPhp/PHPMailer debugging            | boolean|
| http_server| Name of the http server for link creation | string |
| mail_support | Enable notifications per email | boolean |
| mail_server| SMTP Server                               | string |
| mail_auth  | Authentication for SMTP server            | boolean|
| mail_user  | Username of the SMTP server               | string |
| mail_password | Password of the SMTP Server            | string |
| mail_sender_address | Address of the sending email address | string |
| mail_sender_name | Name of the sending email address | string |
| mail_subject | Subject of the notification | string |
| mail_recipient_address | Email address of notification's recipient | string |  

It is also necessary to create the database structure. The file QATool.sql contains the strcuture and can be imported into the sql database.

All files have to be copied into the directory of the web server (for example /var/www/QA). 

When the web server is running the tool can be accessed via http://localhost/QA

## Built With

* [RedBeanPHP](https://www.redbeanphp.com/index.php?p=/changelog#v542) - ORM Framework 
* [Bootstrap 4](https://getbootstrap.com/) - CSS Framework
* [PHPMailer](https://github.com/PHPMailer/PHPMailer) - PHPMailer

## Author

* **Alexander Steinmaurer** - *Concept and Coding* - [Personal Homepage](http://steinmaurer.cc)

## License

This project is open source and available under the [LICENSE.md](LICENSE.md).